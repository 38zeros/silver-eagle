#define _GNU_SOURCE

#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include "hurricane_packet.h"
#include "modbus_config.h"

const int MAX_REGS = 256;

int make_packet(mb_config *cfg, int num_sensors, uint8_t types[], uint32_t readings[]);
void write_packet(mb_config *cfg, int timestamp, char* packet);

// Make a hurricane packet and write the hex representation to a file or stdout.
// default file is (/home/root/readings/<timestamp>_<mission_id>.txt) or stdout if the directory doesn't exist.
int make_packet(mb_config *cfg, int num_sensors, uint8_t types[], uint32_t readings[]) {

    HurricanePacket packet;
    HurricaneSample sample;

    char buf[8192];
    struct timeval tv;
    gettimeofday(&tv,NULL);

    sample.timestamp = tv.tv_sec; //seconds
    sample.readings = readings;

    packet.magic = 0x33385a30;
    packet.type = 5;
    packet.health = 0xff;
    packet.numChannels = 1;
    packet.numSamples = 1;
    packet.numSensors = num_sensors;
    packet.sensorTypes = types;
    packet.samples = &sample;

    encode(&packet, buf);

    write_packet(cfg, tv.tv_sec, buf);
    return EXIT_SUCCESS;
}

void write_packet(mb_config *cfg, int timestamp, char* packet) {
    char *out_fn = NULL;
    FILE *fout;

    asprintf(
        &out_fn,
        "/home/root/readings/%d_%d_%d_modbus_reader.txt",
        timestamp,
        cfg->mission_id,
        cfg->slave_id);

    fout = fopen(out_fn, "w");

    fout ? fputs(packet, fout) : puts(packet);
    fclose(fout);
    free(out_fn);
}

int main(int argc, char *argv[]) {
    mb_config modbus_cfg;
    Config_init(&modbus_cfg);
    int registers[MAX_REGS][2];
    int ch, j, i=0, total_regs=0;
    char *usage = "Usage: %s [-f modbus_read_function_code] [-d data_bits] [-p <parity>] [-s <stop bit>] [-r reg:type] [-r reg:type]... \n\
  -i <mission_id>                - id of the mission that is running \n\
                                   (used for saving the modbus config) \n\
  -f <modbus_read_function_code> - defaults to 4, but can be 3 in some cases \n\
  -a <modbus_address>            - modbus address (aka slave_id) to connect to (supplied via mission, defaults to 1) \n\
  -d <data_bits>                 - data bits to connect the rtu with (7,8 or 9)p\n\
  -p <parity>                    - parity setting ('E', 'O', 'N') \n\
  -s <stop bits>                 - (1, 2 or 3) \n\
  -r <reg>,<num_to_read>         - Specify a register (in decimal) or range of registers to read, can be multiple. \n\
                                   <num_to_read> is 1 if not set \n\
                                   Examples: \n\
                                   -r4,7         -- Read registers 4-11 \n\
                                   -r201,1       -- Read register 201 \n\
                                   -r100 -r2,5   -- Read register 100 and registers 2-7 \n";

    int start_reg=0, num_regs=1;

    while ((ch = getopt(argc, argv, "r:f:d:p:s:i:a:")) != -1) {
        switch (ch) {
            case 'r':
                sscanf(optarg, "%d,%d", &start_reg, &num_regs);
                registers[i][0] = start_reg;
                registers[i][1] = num_regs;
                total_regs += num_regs;
                i++;
                if (i >= MAX_REGS) {
                    fprintf(stderr, "Too many registers specified, max is %d", MAX_REGS);
                    exit(EXIT_FAILURE);
                }
                break;
            case 'i':
                modbus_cfg.mission_id = atoi(optarg);
                break;
            case 'f':
                modbus_cfg.modbus_read_function = atoi(optarg);
                break;
            case 'a':
                modbus_cfg.slave_id = atoi(optarg);
                break;
            case 'd':
                modbus_cfg.data_bits = atoi(optarg);
                break;
            case 'p':
                modbus_cfg.parity = optarg[0];
                break;
            case 's':
                modbus_cfg.stop_bits = atoi(optarg);
                break;
            default:
                printf("Unknown option: %c\n", ch);
                fprintf(stderr, usage, argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if(modbus_cfg.mission_id == CONFIG_DEFAULT) {
        fprintf(stderr, "ERROR: mission_id is required!\n%s", usage);
        exit(EXIT_FAILURE);
    }

    uint32_t readings[total_regs];
    uint8_t types[total_regs];

#if DEBUG
    fprintf(stderr, "Readings: %d\n", total_regs);
#endif

    // fill all types with int.
    for(j=0; j < total_regs; j++)
        types[j] = 2;

    if(connect_modbus(&modbus_cfg) == EXIT_SUCCESS) {
        if(register_readings(&modbus_cfg, registers, i, readings) == EXIT_SUCCESS) {
            return make_packet(&modbus_cfg, total_regs, types, readings);
        } else {
            fprintf(stderr, "ERROR: couldn't get modbus readings\n");
        }
    } else {
        fprintf(stderr, "ERROR: couldn't connect to modbus\n");
    }

    return EXIT_FAILURE;
}
