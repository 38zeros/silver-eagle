#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <modbus-rtu.h>
#include <unistd.h>
#include <string.h>
#include "modbus_config.h"

#define DEVICE "/dev/rs485"

void Config_init(mb_config *cfg) {
	cfg->speed = CONFIG_DEFAULT;
	cfg->data_bits = CONFIG_DEFAULT;
	cfg->stop_bits = CONFIG_DEFAULT;
	cfg->parity = CONFIG_DEFAULT;
	cfg->mission_id = CONFIG_DEFAULT;
	cfg->slave_id = 0x01;
	cfg->modbus_read_function = CONFIG_DEFAULT;
	cfg->ctx = NULL;
}

// Connects to modbus, either by reading the config file, or the
// options, or by probing modbus
//
// Sets the Config.ctx variable to the modbus connection
int connect_modbus(mb_config *cfg)
{
	int ret = EXIT_FAILURE;

	if (_configured(cfg) == EXIT_SUCCESS) {
		modbus_t *ctx;
		ctx = modbus_new_rtu(DEVICE, cfg->speed, cfg->parity, cfg->data_bits,
				cfg->stop_bits);

		struct timeval response_timeout;
		response_timeout.tv_sec = 2;
		response_timeout.tv_usec = 0;
		modbus_set_response_timeout(ctx, &response_timeout);

		if (ctx != NULL) {
			modbus_set_slave(ctx, cfg->slave_id);
			if (modbus_connect(ctx) != -1) {
				cfg->ctx = ctx;
				ret = EXIT_SUCCESS;
			} else {
				/* Close the MODBUS connection */
				modbus_close(ctx);
				modbus_free(ctx);
			}
		}
	} else {
		if ((ret = probe_modbus(cfg)) == EXIT_FAILURE) {
			/* Close the MODBUS connection */
			modbus_close(cfg->ctx);
			modbus_free(cfg->ctx);
		}
	}
	return ret;
}

int _configured(mb_config *cfg)
{
	char *fn = NULL;
	config_filename(&fn, cfg->mission_id);
	if (cfg->parity != CONFIG_DEFAULT && cfg->data_bits != CONFIG_DEFAULT
			&& cfg->speed != CONFIG_DEFAULT) {
		free(fn);
		return EXIT_SUCCESS;
	} else if (_read_config(cfg, fn) == EXIT_SUCCESS) {
		free(fn);
		return EXIT_SUCCESS;
	}

	free(fn);
	return EXIT_FAILURE;
}

// Reads the configuration from config_filename
int _read_config(mb_config *cfg, char *fn)
{
	int ret = EXIT_FAILURE;
	FILE *cfg_file = fopen(fn, "r");
	char key[128];
	char val[128];
	ssize_t linelen;
	size_t linecap = 0;
	char *line = NULL;

	if (cfg_file) {
		while ((linelen = getline(&line, &linecap, cfg_file)) > 0) {

			if (line[0] != EOF) {
				ret = EXIT_SUCCESS;
				sscanf(line, "%s %s", key, val);
				if (strcmp(key, "data_bits:") == 0) {
					cfg->data_bits = atoi(val);
				} else if (strcmp(key, "parity:") == 0) {
					cfg->parity = val[0];
				} else if (strcmp(key, "speed:") == 0) {
					cfg->speed = atoi(val);
				} else if (strcmp(key, "stop_bits:") == 0) {
					cfg->stop_bits = atoi(val);
				} else if (strcmp(key, "slave_id:") == 0) {
					cfg->slave_id = atoi(val);
				} else if (strcmp(key, "modbus_read_function:") == 0) {
					cfg->modbus_read_function = atoi(val);
				}
			} else {
				break;
			}
		}
	}
	free(line);
	return ret;
}

void config_filename(char **fn, int mission_id)
{
    asprintf(fn, "/etc/config/38z/modbus_reader_%d.txt", mission_id);
}

// Write the config to a file
int write_config(mb_config *cfg, char *fn)
{
	FILE *file;

	file = fopen(fn, "w");

	fprintf(file,
			"data_bits: %d\nparity: %c\nspeed: %d\nstop_bits: %d\nmodbus_read_function: %d\nslave_id: %d\n",
			cfg->data_bits, cfg->parity, cfg->speed, cfg->stop_bits,
			cfg->modbus_read_function, cfg->slave_id);

	return fclose(file);
}

// Try different modbus configurations.  If a correct configuration is
// found, save the settings.
int probe_modbus(mb_config *cfg)
{
	int device_speeds[] = { 115200, 19200, 9600 };
	int data_bits[] = { 8, 9, 7 };
	char parity[] = { 'N', 'O', 'E' };
	int stop_bits[] = { 1, 2, 3 };
	mb_config configs[(int) pow(3, 4)];
	modbus_t *ctx;
	int i, db, p, sb, ds;

	for (i = 0, ds = 0; ds < 3; ds++) {
		for (db = 0; db < 3; db++) {
			for (p = 0; p < 3; p++) {
				for (sb = 0; sb < 3; sb++) {
					mb_config c;
					c.speed = device_speeds[ds];
					c.parity = parity[p];
					c.data_bits = data_bits[db];
					c.stop_bits = stop_bits[sb];

					configs[i++] = c;
				}
			}
		}
	}

	for (i = 0; i < (int) pow(3, 4); i++) {
		mb_config c = configs[i];

		/* Set up a new MODBUS context */
		ctx = modbus_new_rtu(DEVICE, c.speed, c.parity, c.data_bits,
				c.stop_bits);
		if (ctx != NULL) {
			modbus_set_slave(ctx, cfg->slave_id);

			/* Open the MODBUS connection */
			if (modbus_connect(ctx) == -1) {
				fprintf(stderr, "Connection failed: %s\n",
						modbus_strerror(errno));
				modbus_free(ctx);
			} else {
				uint16_t reading;
				// Illegal Data Addresses mean a valid connection
				// because we use reg 1 as the register to check.
				if (modbus_read_registers(ctx, 1, 1, &reading)
						== -1&& errno != EMBXILADD) {
					fprintf(stderr,
							"Read Register failed: speed: %d, parity: %c, db: %d sb: %d err: %s\n",
							c.speed, c.parity, c.data_bits, c.stop_bits,
							modbus_strerror(errno));
				} else {
					fprintf(stderr,
							"Connection succeeded: speed: %d, parity: %c, db: %d sb: %d r: %d\n",
							c.speed, c.parity, c.data_bits, c.stop_bits,
							reading);
					cfg->speed = c.speed;
					cfg->parity = c.parity;
					cfg->data_bits = c.data_bits;
					cfg->stop_bits = c.stop_bits;
					cfg->ctx = ctx;
					return EXIT_SUCCESS;
				}
			}
		}
	}
	return EXIT_FAILURE;
}

// read registers from modbus and assemble packet, returns values in
// readings, a buffer which should be as large as num_registers

// This closes the modbus connection, so you'll need to reconnect later if needed
int register_readings(mb_config *cfg, int registers[][2], int num_registers,
		uint32_t *readings)
{
	int ret = EXIT_SUCCESS;
	int curr = 0;
	modbus_t *ctx = cfg->ctx;
	int i;

	for (i = 0; i < num_registers; i++) {
		int reg = registers[i][0];
		int num_regs = registers[i][1];
		uint16_t reg_reads[num_regs];

		for (i = 0; i < num_regs; i++)
			reg_reads[i] = 0;

		switch (cfg->modbus_read_function) {
		case 3:
			if (modbus_read_registers(ctx, reg, num_regs, reg_reads) == -1) {
				fprintf(stderr, "Error reading register: %d err: %s\n", reg,
						modbus_strerror(errno));
				ret = EXIT_FAILURE;
			}
			break;
		case 4:
			if (modbus_read_input_registers(ctx, reg, num_regs, reg_reads)
					== -1) {
				fprintf(stderr, "Error reading register: %d err: %s\n", reg,
						modbus_strerror(errno));
				ret = EXIT_FAILURE;
			}
			break;
		default:
			// try both read methods
			if (modbus_read_input_registers(ctx, reg, num_regs, reg_reads)
					== -1&& errno == EMBXILFUN) {
				if (modbus_read_registers(ctx, reg, num_regs, reg_reads)
						== -1) {
					fprintf(stderr, "Error reading register: %d err: %s\n", reg,
							modbus_strerror(errno));
					ret = EXIT_FAILURE;
				} else {
					cfg->modbus_read_function = 3;
				}
			} else {
				cfg->modbus_read_function = 4;
			}
		}

		for (i = 0; i < num_regs; i++) {
			readings[curr + i] = reg_reads[i];
#if DEBUG
			fprintf(stderr, "register: %d reading: %d\n", reg + i,
					reg_reads[i]);
#endif
		}

		curr += num_regs;
	}

	char *fn = NULL;
	config_filename(&fn, cfg->mission_id);
	write_config(cfg, fn);
	free(fn);

	/* Close the MODBUS connection */
	modbus_close(ctx);
	modbus_free(ctx);

	return ret;
}
