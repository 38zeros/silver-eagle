#ifndef _MODBUS_CONFIG_H_
#define _MODBUS_CONFIG_H_
#include "modbus.h"

#define CONFIG_DEFAULT -1

typedef struct mb_config{
    int speed;
    int data_bits;
    char parity;
    int stop_bits;
    int mission_id;
    int slave_id;
    int modbus_read_function; // some modbus devices use 3 for reading inputs instead of 4
    modbus_t *ctx;
} mb_config;

int probe_modbus(mb_config *cfg);
int connect_modbus(mb_config *cfg);
int write_config(mb_config *cfg, char *fn);
int register_readings(mb_config *cfg, int registers[][2], int num_registers, uint32_t *readings);
int _configured(mb_config *cfg);
int _read_config(mb_config *cfg, char *fn);
void config_filename(char **fn, int mission_id);
void Config_init(mb_config *cfg);


#endif
