# README #

### C application(s) for Silver Eagle Project ###

### How do I get set up? ###

**This code is meant to be used in the context of Intel's iotdk dev environment. I use the Windows 7 & the Mac versions. The Linux version had too many bugs.**

Here are the steps to get Intel's iotdk set up from scratch on a Windows machine (Mac is very silimar):

* Download iotdk-ide-win.7z: [https://software.intel.com/en-us/node/519930](https://software.intel.com/en-us/node/519930)
* Install 7-zip if needed:  [http://www.7-zip.org/](http://www.7-zip.org/)
* Unzip iotdk-ide-win.7z
* Or just go to http://www.intel.com/support/edison/sb/CS-035180.htm for the installer.
* Go to iotdk-ide-win/ and create shortcut for devkit-launcher.bat on your Desktop
* There will be a list of projects already installed like 1_c_helloworld. You can delete all of these.
* Next, import the projects that you wish to work on. For this example we will look at silver-eagle.
* Download the code from silver-eagle from bitbucket:

```
#!bash

$ git clone https://rbaltzer@bitbucket.org/38zeros/silver-eagle.git
```
* Within this codebase, there is a project named mpu6050.
* On the project explorer panel, right click and select "Import..."
* Select General->Existing Projects into Workspace
* Click on the "Select root directory:" radio button (should already be selected) and then click "Browse"
* Select the directory silver-eagle/mpu6050 and click "OK"
* Click on "Finish"
* The mpu6050 project should have imported into eclipse.
* You can build the project by right clicking the project and
selecting "Build Project"
* In Debug Configurations -> C/C++ Remote Applications -> Main -> Connection, created a new SSH connection with your Edison's IP (over wifi) address.
* You can edit, compile and debug over wifi from Eclipse.

### Building Yocto Image Gotchas ###
* If trying to build the kernel, be mindful that the dashes are short, not long which can happen when cutting and pasting.

### Building & Flashing An Image ###

* To build image for flashing, from the edison-src\ directory. (If anything RED appears, there is a problem with the Yocto build):

```
#!bash
$ source poky/oe-init-build-env && bitbake edison-image -v && cd .. && device-software/utils/flash/postBuild.sh
```
* (NOTE: THIS (and the Intel instructions online that also talk about this) DOES NOT WORK ON MAC OS X! -- Use Ubuntu) To flash to Edison, make sure you are connected to the OTG USB port. from the edison-src\build\toFlash\ directory:
```
#!bash
$ sudo ./flashall.sh
```
	NOTE: You may also need gnu-getopt, coreutils, and dfu-util:
		brew update
		brew install gnu-getopt coreutils dfu-util

* Reboot board and flashing should start.
```
#!bash
$ reboot ota
```

* After flashing complete, reconnect OTG USB to the USB hub and power cycle the entire Silver-Eagle.

### Quick Start ###

* Config system (done only once after flashing new image):
```
#!bash
$ /etc/38z/install_config.sh
```

<then reconnect the usb hub and then power cycle entire system>

* To configure SSH (done only once after flashing new image):
```
#!bash
$ configure_edison --setup
```
(don't setup wifi! Set password to 'password')

* Create link through 3G modem by:
```
#!bash
$ pppd -detach call cdma &
```
If connected via wifi (for testing) this is not needed.

* Read some data from modbus by (-a2 is channel 2, mission id is 123456):
```
#!bash
$ modbus -i123456 -r4,5 -a2
```
(In silver-eagle, this might be periodically run by a chron job.)

* Start gathering accelerometer & gyro data from the MPU6050 with sample rate of 500 ms and
  packet creation rate of 15 seconds by:
```
#!bash
$ mpu6050 -i123456 -s 500 -h 15 &
```
(This sample & packet creation rate will produce 30 samples of the accel/gyro which are 3 values each for a total of 180 values per packet.)

* Start gathering GPS data from the Venus chip with sample rate and packet creation rate of 1 second by:
```
#!bash
$ venusGPS -i123456 -h 1 &
```
* To set up reverse SSH tunnel:
```
#!bash
$ /etc/autossh-dir/autossh -M 39000 -y -o "ServerAliveInterval 60" -o "ServerAliveCountMax 3” -N -T -R 23001:localhost:22 root@ssh.thirtyeightzeros.com &
```
* Send the accumulated modbus and mpu6050 readings and  to cloud by:
```
#!bash
$ send_readings.sh
```
(In silver-eagle, this might be periodically run by a chron job. Be sure to empty the readings/sent folder or the flash partition will run out of space eventually.)
* There is a small bash script that runs the mpu6050, venusGPS & the modbus reader concurrently and sends the packets to the cloud:
```
#!bash
$ /etc/38z/test_concurrency.sh
```

### What if I forget my password? ###

If you forget Edison root password and you need to remove it then execute this Arduino sketch:
```
#!c
void setup() {

     system("cp /etc/shadow /etc/shadow.save");

     system("cp /etc/shadow- /etc/shadow");

}

void loop() {

}
```




### Who do I talk to? ###

* Rob Baltzer rob.baltzer01@gmail.com