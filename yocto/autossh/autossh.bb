DESCRIPTION = "Autossh"
SECTION = "base"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "http://www.harding.motd.ca/autossh/autossh-1.4d.tgz"

SRC_URI[md5sum] = "89c09b50aa2d57814f808d727e937d0f"
SRC_URI[sha256sum] = "00008fe458bde4c94e98bfa96e1e6e18c4107a1f9fc8a538556b82e91ddedc16"

inherit autotools

S="${WORKDIR}/autossh-1.4d"

do_compile() {
        cd ${WORKDIR}/autossh-1.4d    
        ./configure
		make
}

do_install() {
        install -v -d  ${D}/etc/autossh-dir/
        install -m 0755 ${WORKDIR}/autossh-1.4d/autossh ${D}/etc/autossh-dir/autossh
}

FILES_${PN}-dbg += "/etc/autossh-dir/.debug"