DESCRIPTION = "Custom scripts, apps and config for 38 Zeros"
SECTION = "support"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESEXTRAPATHS_prepend := "${THISDIR}/files/"

SRC_URI = "	file://send_reading.sh 		\
	   		file://send_readings.sh 	\
	   		file://cdma 				\
	   		file://cdma_chat 			\
	   		file://resolv.conf	 		\
	   		file://mpu6050		 		\
	   		file://venusGPS		 		\
	   		file://test_concurrency.sh	\
	   		file://profile		 		\
	   		file://install_config.sh    \
	   		file://journald.conf    	\
	   		file://id_rsa		    	\
	   		file://id_rsa.pub	    	\
	   		file://modbus"

S = "${WORKDIR}"

#INSANE_SKIP_${PN} = "installed-vs-shipped"

do_install() {
        install -v -d  ${D}/etc/38z/
        install -v -d  ${D}/etc/ppp/peers/
        install -v -d  ${D}/etc/config
        install -v -d  ${D}/etc/config/38z/
        install -v -d  ${D}/etc/systemd/
        
        install -m 0755 ${WORKDIR}/test_concurrency.sh ${D}/etc/38z/test_concurrency.sh
        install -m 0755 ${WORKDIR}/profile ${D}/etc/38z/.profile
        install -m 0755 ${WORKDIR}/venusGPS ${D}/etc/38z/venusGPS
        install -m 0755 ${WORKDIR}/mpu6050 ${D}/etc/38z/mpu6050
        install -m 0755 ${WORKDIR}/modbus ${D}/etc/38z/modbus
        install -m 0755 ${WORKDIR}/send_reading.sh ${D}/etc/38z/send_reading.sh
        install -m 0755 ${WORKDIR}/send_readings.sh ${D}/etc/38z/send_readings.sh
        install -m 0755 ${WORKDIR}/cdma ${D}/etc/ppp/peers/cdma
        install -m 0755 ${WORKDIR}/cdma_chat ${D}/etc/ppp/peers/cdma_chat
        install -m 0755 ${WORKDIR}/resolv.conf ${D}/etc/resolv.conf
        install -m 0755 ${WORKDIR}/install_config.sh ${D}/etc/38z/install_config.sh
        install -m 0755 ${WORKDIR}/journald.conf ${D}/etc/38z/journald.conf
        install -m 0755 ${WORKDIR}/id_rsa ${D}/etc/38z/id_rsa
        install -m 0755 ${WORKDIR}/id_rsa.pub ${D}/etc/38z/id_rsa.pub
}

FILES_${PN}-dbg += "/etc/38z/.debug"

