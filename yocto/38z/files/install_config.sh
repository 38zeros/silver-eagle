#!/bin/sh

systemctl disable edison_config.service
mkdir /home/root/readings
mkdir /home/root/readings/sent
mkdir /home/root/.ssh
cp -av /etc/38z/.profile /home/root/
cp -av /etc/38z/journald.conf /etc/systemd/journald.conf
cp -av /etc/38z/id_rsa /home/root/.ssh
cp -av /etc/38z/id_rsa.pub /home/root/.ssh
chmod 700 /home/root/.ssh/id_rsa
source /home/root/.profile
sync

