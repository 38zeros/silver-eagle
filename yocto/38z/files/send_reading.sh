#!/bin/sh

set -e
# set -x

GUID_FILE='/etc/38z/guid.txt'

usage() {
    echo "\n\
Usage: send_reading <file> [<url>] \n\
\n\
Options: \n\
  <file>  The file you want to send \n\
  <url>   (Optional) Where you want to send the packet \n\
          default: http://in.38z.ro/device \n"
}

# Convert a hex string to a binary hex string and send it via curl
send_packet() {
    file=$1;
    BINSTR=$(sed 's/\(..\)/\\x\1/g' < $file)
    # get mission_id from filename
    MISSION_ID=$(echo $file | awk -F_ '{ print $(NF-1) }')
    if [ -z $MISSION_ID ]; then
        URL="${DEST}/${GUID}";
    else
        URL="${DEST}/${GUID}/${MISSION_ID}";
    fi
	echo $URL;

    echo -e $BINSTR | curl --location --fail --connect-timeout 10 -X POST --data-binary @- $URL;
}

FILE=$1
DEST=$2

if [ -z $DEST ]; then
    DEST='http://in.38z.ro/device';
fi

if [ ! -f $FILE ]; then
    usage;
    exit 1;
fi

if [ -f $GUID_FILE ]; then
    GUID=`cat $GUID_FILE`;
else
    # if we don't have a guid, use the mac
    case "$OSTYPE" in
        darwin*)
            GUID=`ifconfig en0 | grep ether | awk '{print $2}' | sed -e 's/://g' | tr '[:lower:]' '[:upper:]'`
            ;;
        *)
            GUID=`ifconfig wlan0 | grep HWaddr | awk '{print $5}' | sed -e 's/://g' | tr '[:lower:]' '[:upper:]'`
            ;;
    esac
fi

send_packet $FILE;
