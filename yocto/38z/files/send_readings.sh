#!/bin/sh

DEST=$1

SEND_READINGS_RUNNING=`pgrep -f '/bin/sh /etc/38z/send_readings.sh' |wc -l`;
# The subshell above shows up in ps as another instance of
# send_readings running, so we have 2 instances, not 1
if [[ $SEND_READINGS_RUNNING -gt 2 ]]; then
    echo "send_readings already running, exiting";
    exit;
fi

for f in /home/root/readings/*; do
    if [ -f $f ]; then
        /etc/38z/send_reading.sh $f $DEST && rm $f;
    fi
done
