#include "hurricane_packet.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Encode a packet to a hex representation of the data
// a 6-register packet is 91 bytes
void encode(const HurricanePacketSix *packet, char *buf) {

	char pBuf[256];
	int headerSize = 16 + 8 + 4 + 8;
	int i, j;

	snprintf(buf, headerSize + 1, /* include the string terminating char */
	"%08x%04x%02x%04x", packet->magic, packet->type, packet->numSensors, packet->numSamples);

	for (i = 0; i < packet->numSensors; i++) {
		int charLen = sizeof(packet->sensorTypes[0]) * 2;
		snprintf(pBuf, charLen + 1, "%02x", packet->sensorTypes[i]);
		strncat(buf, pBuf, charLen);
	}

	for (i = 0; i < packet->numSamples; i++) {
		HurricaneSampleSix sample = packet->samples[i];
		int charLen = sizeof(sample.timestamp_s) * 8;
		snprintf(pBuf, charLen + 1, "%016lx", (long unsigned int)(sample.timestamp_s));
		strncat(buf, pBuf, charLen);
		charLen = sizeof(sample.timestamp_ns) * 4;
		snprintf(pBuf, charLen + 1, "%08x", sample.timestamp_ns);
		strncat(buf, pBuf, charLen);

		for (j = 0; j < packet->numSensors; j++) {
			int charLen = sizeof(sample.readings[0]) * 4;
			snprintf(pBuf, charLen + 1, "%08x", sample.readings[j]);
			strncat(buf, pBuf, charLen);
		}
	}
}
