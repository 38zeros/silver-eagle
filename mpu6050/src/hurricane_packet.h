#include <stdint.h>

/* so there is a timestamp, a format specifier, and a device ID, and everything is is a black box */
/* deviceID,length,data,crc then everything within could key off the deviceID */

/* Or we could add deviceID,format,length,data,crc but I'm not sure what having the format at the top level would do for us */
/* 00 1E C0 08 F1 A1 */

// uint64_t deviceID;
// uint32_t crc;

#define HURRICANE_MAGIC_NO	(0x33385a30)

typedef struct HurricaneSampleSix {
  int64_t  timestamp_s;
  int32_t  timestamp_ns;
//  uint32_t *readings;
  int32_t *readings;
} HurricaneSampleSix;

typedef struct HurricaneSampleFive {
  int32_t  timestamp;
//  uint32_t *readings;
  int32_t *readings;
} HurricaneSampleFive;

typedef struct HurricanePacketFive {
  uint32_t         magic;
  uint16_t         type;
  uint8_t          health;
  uint8_t          numChannels;
  uint8_t          numSensors;
  uint16_t         numSamples;
  uint8_t         *sensorTypes; /* type of reading to send: 1 for float, 2 for int */
  HurricaneSampleFive *samples;
} HurricanePacketFive;

typedef struct HurricanePacketSix {
  uint32_t         magic;
  uint16_t         type;
  uint8_t          numSensors;
  uint16_t         numSamples; // this is now
  uint8_t         *sensorTypes; /* type of reading to send: 1 for float, 2 for int */
  HurricaneSampleSix *samples;
} HurricanePacketSix;

// Encode a packet to a hex representation of the data
void encode(const HurricanePacketSix *packet, char *out);
