/*
 * This software is provided "as-is." There is no implied nor explicit warranty on this software.
 */

//
//	Includes
//
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include "mpu6050_registers.h"
#include "hurricane_packet.h"

#define DEBUG

//
//	Defines
//
#if ENABLE_ASSERTS
#define ASSERT(x)				assert(x)
#else
#define ASSERT(x)
#endif
#define EDISON_I2C_BUS				(1)
#define CONFIG_DEFAULT				(-1)
#define DEFAULT_SAMPLE_RATE_MS		(500)
#define DEFAULT_PACKET_RATE_SEC		(15)
#define I2C_RETRIES_				(10)
#define ARRAY_SIZE(x) (sizeof((x)) / sizeof((x)[0]))

//
//	Static Data
//
static uint32_t g_i2c_file;


//
//	Static Functions
//
static void 	open_device(void);
static uint8_t 	read_whoami_reg(void);
static uint8_t 	read_register(uint8_t sub_addr);
static void  	write_register(uint8_t sub_addr, uint8_t value);
static void 	write_bitfield(uint8_t sub_addr, uint8_t bit_start, uint8_t length, uint8_t data);
static void 	initialize_mpu6050(void);
static void 	set_clock_source(uint8_t source);
static void 	set_full_scale_gyro_range(uint8_t range);
static void 	set_full_scale_accel_range(uint8_t range);
static void 	set_sleep_mode(bool enable);


static void 	read_registers(uint8_t* data, uint8_t sub_addr, uint8_t length);
static void 	get_raw_accel_gyro(int32_t* AccelGyro);
static int 		make_packet(uint32_t mission_id, int num_sensors, int num_samples, uint8_t types[], struct timespec, HurricaneSampleSix readings[]);
static void 	write_packet(uint32_t mission_id, struct timespec, char* packet);
#ifdef EVER_NEEDED_DEFINE_ME
static bool 	get_sleep_mode(void);
#endif
#ifdef DEBUG
static uint8_t 	read_bitfield(uint8_t sub_addr, uint8_t bit_start, uint8_t length);
#endif


/** MAIN **/
#define NUM_SENSORS	(6)	// 3 (accel values) + 3 (gyro values) = 6

int main(int argc, char *argv[]) {
	HurricaneSampleSix * samples;
    int32_t readings[NUM_SENSORS];
    uint8_t* types;
	int ch, ii;
	int32_t	sample_index = 0;
	uint32_t number_of_samples = 0, packet_rate_sec = 0, sample_rate_ms = 0, mission_id = 0;
	struct timespec tv;

    char *usage = "Usage: mpu6050 [-i <mission_id>] [-s <sample_rate_ms>] [-h <packet_rate_sec>] \n\
  -i <mission_id>                - id of the mission that is running \n\
  -s <sample_rate_ms>            - The rate at which to poll the mpu6050 in milliseconds\n\
  -h <packet_rate_sec>           - The rate at which to save off hurricane packets to the filesystem\n";

    mission_id = CONFIG_DEFAULT;
    sample_rate_ms = DEFAULT_SAMPLE_RATE_MS;
    packet_rate_sec = DEFAULT_PACKET_RATE_SEC;

    open_device();

    if (read_whoami_reg() != MPU6050_ADDRESS_AD0_LOW) {
    	printf("Reading WHO_AM_I register failed.\n");
        exit(EXIT_FAILURE);
    }

    while ((ch = getopt(argc, argv, "ci:s:h:")) != -1) {
        switch (ch) {
            case 'i':
                mission_id = atoi(optarg);
                break;
            case 's':
            	sample_rate_ms = atoi(optarg);
                break;
            case 'h':
            	packet_rate_sec = atoi(optarg);
                break;
            default:
                printf("Unknown option: %c\n", ch);
                fprintf(stderr, usage, argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if(mission_id == CONFIG_DEFAULT) {
        fprintf(stderr, "ERROR: mission_id is required!\n%s", usage);
        exit(EXIT_FAILURE);
    }

    initialize_mpu6050();

	number_of_samples = (packet_rate_sec * 1000)/sample_rate_ms;
	samples = (struct HurricaneSampleSix *) calloc((NUM_SENSORS+sizeof(HurricaneSampleSix))*number_of_samples, sizeof(int32_t));
	printf("samples: %d", (NUM_SENSORS+sizeof(HurricaneSampleSix))*number_of_samples);
	types = (uint8_t*) calloc(NUM_SENSORS, sizeof(uint8_t));
	memset(types, 2, NUM_SENSORS);


	for (ii = 0; ii < number_of_samples; ii++) {
		samples[ii].readings = (int32_t *) calloc(NUM_SENSORS, sizeof(int32_t));
	}

	sample_index = 0;

	printf("mpu6050 app running...\n");
	while(1) {
		clock_gettime(CLOCK_REALTIME, &tv);
		get_raw_accel_gyro(readings);
		//memcpy((int32_t*)(&samples[sample_index] + 4, &tv, sizeof(struct timespec)); doesn't work
		samples[sample_index].timestamp_s = tv.tv_sec;
		samples[sample_index].timestamp_ns = tv.tv_nsec;
		memcpy((int32_t*)samples[sample_index].readings, readings, ARRAY_SIZE(readings)*4);

#ifdef DEBUG
		printf("raw: %5d %5d %5d %5d %5d %5d\n", samples[sample_index].readings[0], samples[sample_index].readings[1], samples[sample_index].readings[2],samples[sample_index].readings[3],samples[sample_index].readings[4],samples[sample_index].readings[5]);
#endif
		set_full_scale_gyro_range(MPU6050_GYRO_FS_250);
		set_full_scale_accel_range(MPU6050_ACCEL_FS_16);
		usleep (sample_rate_ms * 1000);
		sample_index++;
		if (sample_index >= number_of_samples) {

			make_packet(mission_id, NUM_SENSORS, number_of_samples, types, tv, samples);
			sample_index = 0;
		}
	}
	free(samples);
	free(types);
	for (ii = 0; ii < number_of_samples; ii++) {
			free(samples[ii].readings);
	}
    return 0;
}

// Make a hurricane packet and write the hex representation to a file or stdout.
// default file is (/home/root/readings/<timestamp>_<mission_id>.txt) or stdout if the directory doesn't exist.
int make_packet(uint32_t mission_id, int num_sensors, int num_samples, uint8_t types[], struct timespec tv, HurricaneSampleSix hsamples[]) {
    HurricanePacketSix packet;
    char * buf;

    buf = (char*) calloc(256 + num_sensors*num_samples*32, sizeof(char));

    packet.magic = HURRICANE_MAGIC_NO;
    packet.type = 6;
    packet.numSamples = num_samples;
    packet.numSensors = num_sensors;
    packet.sensorTypes = types;
    packet.samples = hsamples;

    encode(&packet, buf);

    write_packet(mission_id, tv, buf);

    free(buf);
    return EXIT_SUCCESS;
}

void write_packet(uint32_t mission_id, struct timespec tv, char* packet)
{
	char out_fn[1000];
    FILE *fout;

    sprintf(
        out_fn,
        "/home/root/readings/%ld-%ld_%d_mpu6050.txt",
        (long)(tv.tv_sec),
        (long)(tv.tv_nsec),
        mission_id);

    fout = fopen(out_fn, "w");
#ifdef DEBUG
    printf("Raw packet: %s\n", packet);
#endif
    fout ? fputs(packet, fout) : puts(packet);

    fclose(fout);
}

#define FILENAME_SZ	20
static void open_device(void)
{
	  char filename[FILENAME_SZ];

	  // Open i2c device
	  snprintf(filename, FILENAME_SZ-1, "/dev/i2c-%d", EDISON_I2C_BUS);
	  g_i2c_file = open(filename, O_RDWR);
	  ASSERT(g_i2c_file >= 0);

	  // Set i2c address
	  if (ioctl(g_i2c_file, I2C_SLAVE, MPU6050_ADDRESS_AD0_LOW) < 0) {
		  ASSERT(0);
	  }
}

static void initialize_mpu6050(void)
{
    set_clock_source(MPU6050_CLOCK_PLL_XGYRO);
    set_full_scale_gyro_range(MPU6050_GYRO_FS_250);
    set_full_scale_accel_range(MPU6050_ACCEL_FS_16);

	write_register(MPU6050_RA_INT_PIN_CFG, 	(0 << MPU6050_INTCFG_INT_LEVEL_BIT) +
											(1 << MPU6050_INTCFG_INT_OPEN_BIT) +
											(0 << MPU6050_INTCFG_LATCH_INT_EN_BIT));

	write_register(MPU6050_RA_MOT_THR, 0x10);

	write_register(MPU6050_RA_INT_ENABLE, 	1 << MPU6050_INTERRUPT_MOT_BIT);
	write_register(MPU6050_RA_INT_ENABLE, 	(1 << MPU6050_INTERRUPT_DATA_RDY_BIT));

    set_sleep_mode(false);
}

static void set_clock_source(uint8_t source)
{
    write_bitfield(MPU6050_RA_PWR_MGMT_1, MPU6050_PWR1_CLKSEL_BIT, MPU6050_PWR1_CLKSEL_LENGTH, source);
}

static void set_full_scale_gyro_range(uint8_t range)
{
	write_bitfield(MPU6050_RA_GYRO_CONFIG, MPU6050_GCONFIG_FS_SEL_BIT, MPU6050_GCONFIG_FS_SEL_LENGTH, range);
}

static void set_full_scale_accel_range(uint8_t range)
{
	write_bitfield(MPU6050_RA_ACCEL_CONFIG, MPU6050_ACONFIG_AFS_SEL_BIT, MPU6050_ACONFIG_AFS_SEL_LENGTH, range);
}

// true = sleep mode on, false = sleep mode off
static void set_sleep_mode(bool enable)
{
	write_bitfield(MPU6050_RA_PWR_MGMT_1, MPU6050_PWR1_SLEEP_BIT, 1, enable);
}

#ifdef EVER_NEEDED_DEFINE_ME
static bool get_sleep_mode(void)
{
    uint8_t tmp = read_bitfield(MPU6050_RA_PWR_MGMT_1, MPU6050_PWR1_SLEEP_BIT, 1);
    return tmp == 0x00 ? false : true;
}
#endif

#ifdef DEBUG
static uint8_t read_bitfield(uint8_t sub_addr, uint8_t bit_start, uint8_t length)
{
    uint8_t tmp = read_register(sub_addr);
    uint8_t mask = ((1 << length) - 1) << (bit_start - length + 1);
    tmp &= mask;
    tmp >>= (bit_start - length + 1);
    return tmp;
}
#endif

static void get_raw_accel_gyro(int32_t* AccelGyro)
{
	uint8_t tmpBuffer[14];
	int i;

	read_registers(tmpBuffer, MPU6050_RA_ACCEL_XOUT_H, 14);
    for (i = 0; i < 3; i++) {
        AccelGyro[i] = (int32_t) ((int16_t) ((uint16_t) tmpBuffer[2 * i] << 8) + tmpBuffer[2 * i + 1]);
    }
    for (i = 4; i < 7; i++) {
        AccelGyro[i - 1] = (int32_t) ((int16_t) ((uint16_t) tmpBuffer[2 * i] << 8) + tmpBuffer[2 * i + 1]);
    }
}

// Read the who_am_i register. It should match the I2C address
static uint8_t read_whoami_reg(void)
{
	  int32_t retVal;

	  retVal = read_register(MPU6050_RA_WHO_AM_I);
	  ASSERT(retVal == MPU6050_ADDRESS_AD0_LOW);

	  return retVal;
}

static uint8_t read_register(uint8_t sub_addr)
{
	int32_t retVal;
	uint8_t retries = I2C_RETRIES_;

	retVal = i2c_smbus_read_byte_data(g_i2c_file, sub_addr);
	if (retVal != -1) {
		return (uint8_t) (retVal & 0xFF);
	}
	retries--;

	do {
		usleep(1000*100);	// Sleep 100 ms
		retVal = i2c_smbus_read_byte_data(g_i2c_file, sub_addr);
	} while(retries-- && (retVal == -1));

	if (retVal == -1) {
		printf("read_register_retries failed\n");
	}
	return (uint8_t) (retVal & 0xFF);
}

static void read_registers(uint8_t* data, uint8_t sub_addr, uint8_t length)
{
	int32_t read_bytes;
	uint8_t retries = I2C_RETRIES_;

	read_bytes = i2c_smbus_read_i2c_block_data(g_i2c_file, sub_addr, length, data);
	if (read_bytes != -1) {
		return;
	}
	retries--;

	do {
		usleep(1000*100);	// Sleep 100 ms
		read_bytes = i2c_smbus_read_i2c_block_data(g_i2c_file, sub_addr, length, data);
	} while(retries-- && (read_bytes == -1));

	if (read_bytes == -1) {
		printf("read_registers failed\n");
	}

}

static void write_register(uint8_t sub_addr, uint8_t value)
{
	int32_t retVal;
	uint8_t retries = I2C_RETRIES_;

	retVal = i2c_smbus_write_byte_data(g_i2c_file, sub_addr, value);
	if (retVal != -1) {
		return;
	}
	retries--;

	do {
		usleep(1000*100);	// Sleep 100 ms
		retVal = i2c_smbus_write_byte_data(g_i2c_file, sub_addr, value);
	} while(retries-- && (retVal == -1));

	if (retVal == -1) {
		printf("write_register failed\n");
	}
}

static void write_bitfield(uint8_t sub_addr, uint8_t bit_start, uint8_t length, uint8_t data)
{
    uint8_t tmp = read_register(sub_addr);
    uint8_t mask = ((1 << length) - 1) << (bit_start - length + 1);
    printf("write_bitfield 0x%x = 0x%x << %d \n", sub_addr, data, bit_start - length + 1);
    data <<= (bit_start - length + 1);
    data &= mask;
    tmp &= ~(mask);
    tmp |= data;
    write_register(sub_addr, tmp);

#ifdef DEBUG
    // This comparison is wrong, but we can at least see the register value.
    uint8_t new_val = read_register(sub_addr);
	  if (new_val != data) {
		  printf("write_bitfield mismatch 0x%x = 0x%x, not 0x%x as written.\n", sub_addr, new_val, data);
	  }
#endif
}


