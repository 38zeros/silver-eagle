/*
 * mma8452.c
 *
 *  Created on: Dec 20, 2014
 *      Author: rbaltzer
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include "mma8452_registers.h"

//
//	Typedefs
//
typedef uint8_t byte;

//
//	Defines
//
#define ASSERT(x)			assert(x)
#define EDISON_I2C_BUS		(1)
// The SparkFun breakout board defaults to 1, set to 0 if SA0 jumper on the bottom of the board is set
#define MMA8452_ADDRESS 	0x1D  // 0x1D if SA0 is high, 0x1C if low
//Define a few of the registers that we will be accessing on the MMA8452
#define OUT_X_MSB 			0x01
#define XYZ_DATA_CFG  		0x0E
#define WHO_AM_I   			0x0D
#define CTRL_REG1  			0x2A
#define GSCALE 				2 // Sets full-scale range to +/-2, 4, or 8g. Used to calc real g values.

//
//	Static Data
//
static uint32_t file;

//
//	Static Functions
//
static void setup(void);
static void open_device(void);
static void initMMA8452(void);
static uint8_t read_register(uint8_t sub_addr);
static void read_registers(uint8_t sub_addr, uint8_t length, uint8_t* data);
static void write_register(uint8_t sub_addr, uint8_t value);
static void readAccelData(int *destination);static void readAccelData(int *destination);
static void MMA8452Standby(void);
static void MMA8452Active(void);
static void loop(void);

/** MAIN **/
int main(int argc, char *argv[])
{
	setup();
	exit(EXIT_SUCCESS);
}

static void setup(void)
{
  open_device();
  initMMA8452(); //Test and intialize the MMA8452
  while(1) loop();
}

static void open_device(void)
{
	  char filename[20];

	  // Open i2c device
	  snprintf(filename, 19, "/dev/i2c-%d", EDISON_I2C_BUS);
	  file = open(filename, O_RDWR);
	  ASSERT(file >= 0);

	  // Set i2c address
	  if (ioctl(file, I2C_SLAVE, MMA8452_ADDRESS) < 0) {
		  ASSERT(0);
	  }
}

static void loop(void)
{
  int accelCount[3];  // Stores the 12-bit signed value
  int i;
  float accelG[3];  // Stores the real accel value in g's

  readAccelData(accelCount);  // Read the x/y/z adc values

  // Now we'll calculate the accleration value into actual g's

  for (i = 0 ; i < 3 ; i++)
  {
    accelG[i] = (float) accelCount[i] / ((1<<12)/(2*GSCALE));  // get actual g value, this depends on scale being set
  }

  // Print out values
  for (i = 0 ; i < 3 ; i++)
  {
	  printf(" %1.4f", accelG[i]);
  }
  printf("\n");

	usleep(300);
}

static void readAccelData(int *destination)
{
  byte rawData[6];  // x/y/z accel register data stored here
  int i;

  read_registers(OUT_X_MSB, 6, rawData);  // Read the six raw data registers into data array

  // Loop to calculate 12-bit ADC and g value for each axis
  for(i = 0; i < 3 ; i++)
  {
    int gCount = (rawData[i*2] << 8) | rawData[(i*2)+1];  //Combine the two 8 bit registers into one 12-bit number
    gCount >>= 4; //The registers are left align, here we right align the 12-bit integer

    // If the number is negative, we have to make it so manually (no 12-bit data type)
    if (rawData[i*2] > 0x7F)
    {
      gCount = ~gCount + 1;
      gCount *= -1;  // Transform into negative 2's complement #
    }

    destination[i] = gCount; //Record this gCount into the 3 int array
  }
}

// Initialize the MMA8452 registers
// See the many application notes for more info on setting all of these registers:
// http://www.freescale.com/webapp/sps/site/prod_summary.jsp?code=MMA8452Q
static void initMMA8452(void)
{
  byte c = read_register(WHO_AM_I);  // Read WHO_AM_I register
  if (c == 0x2A) // WHO_AM_I should always be 0x2A
  {
    printf("MMA8452Q is online...\n");
  }
  else
  {
    printf("Could not connect to MMA8452Q: 0x%x", c);
    while(1) ; // Loop forever if communication doesn't happen
  }

  MMA8452Standby();  // Must be in standby to change registers

  // Set up the full scale range to 2, 4, or 8g.
  byte fsr = GSCALE;
  if(fsr > 8) fsr = 8; //Easy error check
  fsr >>= 2; // Neat trick, see page 22. 00 = 2G, 01 = 4A, 10 = 8G
  write_register(XYZ_DATA_CFG, fsr);

  //The default data rate is 800Hz and we don't modify it in this example code

  MMA8452Active();  // Set to active to start reading
}

// Sets the MMA8452 to standby mode. It must be in standby to change most register settings
static void MMA8452Standby(void)
{
  byte c = read_register(CTRL_REG1);
  write_register(CTRL_REG1, c & ~(0x01)); //Clear the active bit to go into standby
}

// Sets the MMA8452 to active mode. Needs to be in this mode to output data
static void MMA8452Active(void)
{
  byte c = read_register(CTRL_REG1);
  write_register(CTRL_REG1, c | 0x01); //Set the active bit to begin detection
}

static void read_registers(uint8_t sub_addr, uint8_t length, uint8_t* data)
{
	  int32_t read_bytes;

	  read_bytes = i2c_smbus_read_i2c_block_data(file, sub_addr, length, data);
	  if (read_bytes != length) {
		  printf("read_bytes %d length %d\n", read_bytes, length);
		  ASSERT(0);
	  }
	  //ASSERT(read_bytes != length);
}

static uint8_t read_register(uint8_t sub_addr)
{
	  int32_t retVal;

	  retVal = i2c_smbus_read_byte_data(file, sub_addr);
	  ASSERT(retVal != -1);

	  return (uint8_t) (retVal & 0xFF);
}

static void write_register(uint8_t sub_addr, uint8_t value)
{
	  int32_t retVal;

	  retVal = i2c_smbus_write_byte_data(file, sub_addr, value);
	  ASSERT(retVal != -1);

#ifdef DEBUG
	  if (read_register(sub_addr) != value) {
		  printf("write_register mismatch 0x%x = 0x%x\n", sub_addr, value);
	  }
#endif
}
