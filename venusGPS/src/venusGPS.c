/*
 * This software is provided "as-is." There is no implied nor explicit warranty on this software.
 */

//
//	Includes
//
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/time.h>
#include <unistd.h>
#include <termios.h>
#include "info.h"
#include "parser.h"
#include "datalogger.h"
#include "lowlevel.h"
#include "hurricane_packet.h"

#define CONFIG_DEFAULT			(-1)
#define DEFAULT_PACKET_RATE_SEC	(1)	// Can be a value from 1 to 255 (1- 255 seconds/packet)

//#define DEBUG_NO_SATELLITES	(1)

//
//	Local Functions
//
static void open_device(void);
#if DEBUG
static void parse_test(void);
#endif
static void loop(void);
static int make_packet(uint32_t mission_id, int num_sensors, uint8_t types[], int32_t readings[]);
static void write_packet(uint32_t mission_id, int timestamp, char* packet);

//
//	Local Data
//
static FILE* fd;
static int fd2;
static uint32_t mission_id;
static uint8_t packet_rate_sec;


//
//	** MAIN **
//
int main(int argc, char *argv[])
{

	int baud_rate = 0;
	int ch;
    char *usage = "Usage: venusGPS [-i <mission_id>] [-h <packet_rate_sec>] \n\
  -i <mission_id>                - id of the mission that is running \n\
  -h <packet_rate_sec>           - The rate at which to save off hurricane packets to the filesystem\n";

    mission_id = CONFIG_DEFAULT;
    packet_rate_sec = DEFAULT_PACKET_RATE_SEC;

    while ((ch = getopt(argc, argv, "ci:h:")) != -1) {
        switch (ch) {
            case 'i':
                mission_id = atoi(optarg);
                break;
            case 'h':
            	packet_rate_sec = atoi(optarg);
                break;
            default:
                printf("Unknown option: %c\n", ch);
                fprintf(stderr, usage, argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if(mission_id == CONFIG_DEFAULT) {
        fprintf(stderr, "ERROR: mission_id is required!\n%s", usage);
        exit(EXIT_FAILURE);
    }


	fd2 = open_port( "/dev/ttyMFD1");
	if ( fd2 == -1 ) {
	  fprintf(stderr,"Failed to open device %s\n",  "/dev/ttyMFD1");
	  return 1;
	}


	// detect device and speed
	if ( baud_rate == 0 ) {
	  baud_rate = skytraq_determine_speed(fd2);
	  if ( baud_rate == 0 ) {
		  fprintf(stderr,"Could not find data logger at port %s\n",  "/dev/ttyMFD1");
		  return -1;
	  }
	}

	skytraq_read_software_version(fd2);
	skytraq_configure_nmea_message(packet_rate_sec, fd2);
	skytraq_set_navigation_mode_to_vehicle(fd2);
	close(fd2);
	loop();
	return 0;
}

#define TOTAL_VALUES			(6)
#define FIXED_POINT_MULTIPLIER	(1000)
static void loop(void)
{
	char * line = NULL;
    size_t len = 0;
    ssize_t read;
    nmeaINFO info;
    nmeaPARSER parser;
    int32_t readings[TOTAL_VALUES];
    uint8_t types[TOTAL_VALUES];
    int i;

    for (i = 0 ; i < TOTAL_VALUES ; i++ ) {
    	types[i] = 2;	// All fixed point floats -- send as ints.
    }
    nmea_zero_INFO(&info);
    nmea_parser_init(&parser);
	open_device();

#ifdef DEBUG_NO_SATELLITES
	// Debug - use this when no coverage
    const char *buff = {
            "$GPRMC,111609.14,A,5001.27,N,3613.06,E,11.2,0.0,261206,0.0,E*50\r\n",
    };
#endif

	while (1)
	{
		while ((read = getline(&line, &len, fd)) != -1)
		{
			//printf("%s", line);	// Debug
#ifdef DEBUG_NO_SATELLITES
	        nmea_parse(&parser, buff, (int)strlen(buff), &info);
#else
	        nmea_parse(&parser, line, (int)strlen(line), &info);
#endif
//	        printf("lat %f lon %f elv %f speed %f direction %f declination %f\n", info.lat, info.lon, info.elv, info.speed, info.direction, info.declination);
	        readings[0] = (int32_t) (info.lat*FIXED_POINT_MULTIPLIER);
	        readings[1] = (int32_t) (info.lon*FIXED_POINT_MULTIPLIER);
	        readings[2] = (int32_t) (info.elv*FIXED_POINT_MULTIPLIER);
	        readings[3] = (int32_t) (info.speed*FIXED_POINT_MULTIPLIER);
	        readings[4] = (int32_t) (info.direction*FIXED_POINT_MULTIPLIER);
	        readings[5] = (int32_t) (info.declination*FIXED_POINT_MULTIPLIER);
	        make_packet(mission_id, TOTAL_VALUES, types, readings);
		}
	}
}

static void open_device(void)
{
	fd = fopen("/dev/ttyMFD1", "rw" );
}

// Make a hurricane packet and write the hex representation to a file or stdout.
// default file is (/home/root/readings/<timestamp>_<mission_id>.txt) or stdout if the directory doesn't exist.
static int make_packet(uint32_t mission_id, int num_sensors, uint8_t types[], int32_t readings[])
{

    HurricanePacket packet;
    HurricaneSample sample;

    char buf[8192];
    struct timeval tv;
    gettimeofday(&tv,NULL);

    sample.timestamp = tv.tv_sec; //seconds
    sample.readings = readings;

    packet.magic = 0x33385a30;
    packet.type = 5;
    packet.health = 0xff;
    packet.numChannels = 1;
    packet.numSamples = 1;
    packet.numSensors = num_sensors;
    packet.sensorTypes = types;
    packet.samples = &sample;

    encode(&packet, buf);

    write_packet(mission_id, tv.tv_sec, buf);

    return EXIT_SUCCESS;
}

static void write_packet(uint32_t mission_id, int timestamp, char* packet)
{

	char out_fn[1000];
    FILE *fout;

    sprintf(
        out_fn,
        "/home/root/readings/%d_%d_venusGPS.txt",
        timestamp,
        mission_id);

    fout = fopen(out_fn, "w");
#if DEBUG
    printf("Raw packet: %s\n", packet);
#endif
    fout ? fputs(packet, fout) : puts(packet);

    fclose(fout);

}

#if DEBUG
static void parse_test(void)
{
    const char *buff[] = {
        "$GPRMC,173843,A,3349.896,N,11808.521,W,111.0,360.0,230108,013.4,E*69\r\n",
        "$GPGGA,111609.14,5001.27,N,3613.06,E,3,08,0.0,10.2,M,0.0,M,0.0,0000*70\r\n",
        "$GPGSV,2,1,08,01,05,005,80,02,05,050,80,03,05,095,80,04,05,140,80*7f\r\n",
        "$GPGSV,2,2,08,05,05,185,80,06,05,230,80,07,05,275,80,08,05,320,80*71\r\n",
        "$GPGSA,A,3,01,02,03,04,05,06,07,08,00,00,00,00,0.0,0.0,0.0*3a\r\n",
        "$GPRMC,111609.14,A,5001.27,N,3613.06,E,11.2,0.0,261206,0.0,E*50\r\n",
        "$GPVTG,217.5,T,208.8,M,000.00,N,000.01,K*4C\r\n"
    };

    int it;
    nmeaINFO info;
    nmeaPARSER parser;

    nmea_zero_INFO(&info);
    nmea_parser_init(&parser);

    for(it = 0; it < 6; ++it) {
        nmea_parse(&parser, buff[it], (int)strlen(buff[it]), &info);
        printf("lat %f\n", info.lat);
    }

    nmea_parser_destroy(&parser);
}
#endif
