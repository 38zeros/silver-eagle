#include "hurricane_packet.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Encode a packet to a hex representation of the data
// a 6-register packet is 91 bytes
void encode(const HurricanePacket *packet, char *buf) {

	char pBuf[32];
	int headerSize = 16 + 8 + 4 + 4 + 4 + 8;
	int i, j;

	snprintf(buf, headerSize + 1, /* include the string terminating char */
	"%08x%04x%02x%02x%02x%04x", packet->magic, packet->type, packet->health,
			packet->numChannels, packet->numSensors, packet->numSamples);

	for (i = 0; i < packet->numSensors; i++) {
		int charLen = sizeof(packet->sensorTypes[0]) * 4;
		snprintf(pBuf, charLen + 1, "%02x", packet->sensorTypes[i]);
		strncat(buf, pBuf, charLen);
	}

	for (i = 0; i < packet->numSamples; i++) {
		HurricaneSample sample = packet->samples[i];

		int charLen = sizeof(sample.timestamp) * 4;
		snprintf(pBuf, charLen + 1, "%08x", sample.timestamp);
		strncat(buf, pBuf, charLen);

		for (j = 0; j < packet->numSensors; j++) {
			int charLen = sizeof(sample.readings[0]) * 4;
			snprintf(pBuf, charLen + 1, "%08x", sample.readings[j]);
			strncat(buf, pBuf, charLen);
		}
	}
}
